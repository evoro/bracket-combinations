//*****************************************************************************
// File name: braces-combinations.h
// Creation date: 18.12.2022
// Author: Evgeny Voropaev evorop@gmail.com
//*****************************************************************************

/*
 * Есть N открывающихся и закрывающихся скобок.
 * Необходимо вычислить общее количество всех комбинаций по правилам арифметики.
 * Ну тоесть каждой  "(" соответствует одна ")".
 *
 * Например если  N=2:
 * (())
 * () ()
 * Две комбинации.
*/
#pragma once
#include <iostream>
#include <vector>
#include <unordered_map>
#include <cassert>
#include <string>
//-------------------------------------------------------------------------------------------------
using namespace std;
//-------------------------------------------------------------------------------------------------
using BracesCombsT = vector<vector<uint8_t>>;
constexpr uint8_t bra = '(';
constexpr uint8_t ket = ')';
//-------------------------------------------------------------------------------------------------

BracesCombsT& BracesCombs(uint32_t N)
{
  static unordered_map<uint32_t, BracesCombsT> cache;

  if(!cache.count(N)) // AAA
  {
    BracesCombsT res;

    for(uint32_t r = 1; r < 2*N; r = r+2)
    {
      uint32_t leftN = (r - 1) / 2;
      BracesCombsT leftCombs = BracesCombs(leftN);
      assert((r == 1 && leftCombs.size() == 0 ) || r > 1);

      uint32_t rightN = (2*N - r - 1) / 2;
      BracesCombsT rightCombs = BracesCombs(rightN);

      BracesCombsT::const_iterator left = leftCombs.cbegin();
      do // XXX
      {
        vector<uint8_t> comb(2*N);
        comb[0] = bra;
        comb[r] = ket;

        assert( (left == leftCombs.cend() && r == 1) || (left != leftCombs.cend() && r > 1 && left->size() == r-1) );

        for(size_t xl = 0; left != leftCombs.cend() && xl < left->size(); xl++)
          comb[xl+1] = left->at(xl);

        if(rightCombs.size())
          for(const auto& right: rightCombs)
          {
            for(size_t xr = 0; xr < right.size(); xr++)
              comb[r+1+xr] = right.at(xr);
            res.push_back(comb);
          }
        else
          res.push_back(comb);

      } while(left++ != leftCombs.cend() && left != leftCombs.cend()); // XXX
    }

    cache[N] = move(res);
  } // AAA
  return cache[N];
}

vector<string> PrintBraceCombs(BracesCombsT& combs, bool toConsole = false)
{
  vector<string> strvec;
  for(const auto& comb: combs)
  {
    string str;
    str.reserve(comb.size());

    for(const auto& symb: comb)
      str.push_back((char)symb);

    if(toConsole)
      cout << str << endl;

    strvec.push_back(str);
  }
  return strvec;
}

int test()
{
  vector CatalansNumbers { 1, 1, 2, 5, 14, 42, 132, 429, 1430, 4862,
                           16796, 58786, 208012, 742900, 2674440,
                           9694845, 35357670, 129644790 };

  {
    auto bc = BracesCombs(1);
    assert(bc.size() == CatalansNumbers[1]);
    auto strvec = PrintBraceCombs(bc);
    assert(strvec[0] == "()");
  }
  {
    auto bc = BracesCombs(2);
    assert(bc.size() == CatalansNumbers[2]);
    auto strvec = PrintBraceCombs(bc);
    assert(strvec[0] == "()()");
    assert(strvec[1] == "(())");
  }
  {
    auto bc = BracesCombs(3);
    assert(bc.size() == CatalansNumbers[3]);
    auto strvec = PrintBraceCombs(bc);
    assert(strvec[0] == "()()()");
    assert(strvec[1] == "()(())");
    assert(strvec[2] == "(())()");
    assert(strvec[3] == "(()())");
    assert(strvec[4] == "((()))");
  }
  {
    auto bc = BracesCombs(4);
    assert(bc.size() == CatalansNumbers[4]);
    auto strvec = PrintBraceCombs(bc);
    assert(strvec[0] == "()()()()");
    assert(strvec[1] == "()()(())");
    assert(strvec[2] == "()(())()");
    assert(strvec[3] == "()(()())");
    assert(strvec[4] == "()((()))");
    assert(strvec[5] == "(())()()");
    assert(strvec[6] == "(())(())");
    assert(strvec[7] == "(()())()");
    assert(strvec[8] == "((()))()");
    assert(strvec[9] == "(()()())");
    assert(strvec[10] == "(()(()))");
    assert(strvec[11] == "((())())");
    assert(strvec[12] == "((()()))");
    assert(strvec[13] == "(((())))");
  }

  for(auto i: {5, 6, 7, 8, 9, 10, 11, /*12,13,14,15,16,17*/})
  {
    auto bc = BracesCombs(i);
    assert(bc.size() == CatalansNumbers[i]);
  }

  return 0;
}
