//*****************************************************************************
// File name: main.cpp
// Creation date: 18.12.2022
// Author: Evgeny Voropaev evorop@gmail.com
//*****************************************************************************
#include "braces-combinations.h"
//-------------------------------------------------------------------------------------------------
using namespace std;
//-------------------------------------------------------------------------------------------------

int main()
{
  cout << "Tests start ..." << endl;
  test();
  cout << "Tests have been  passed [OK]." << endl << endl;

  unsigned int n;
  cout << endl <<"Enter braces pairs number (n) ...";
  cin >> n;
  cout << endl << "n = " << n << endl;
  PrintBraceCombs(BracesCombs(n), true);
  cout << endl << "Combinations quantity = " << BracesCombs(n).size() << endl;
}
